// ELement references.
const balance = document.getElementById("balance")
const workPayBalanceElement = document.getElementById("workPayBalance")
const debtElement = document.getElementById("debt")
const bankDebtElement = document.getElementById("bankDebt")
const repayLoanButton = document.getElementById("repayLoan")
const laptopsElement = document.getElementById("laptops")
const priceElement = document.getElementById("price")
const specsElement = document.getElementById("specs")
const imageElement = document.getElementById("image")
const descriptionElement = document.getElementById("description")

// Functions to show or hide elements.
const hideElement = (element) => { element.style.display = "none" }
const showElement = (element) => { element.style.display = "block" }

// Define and set starting values.
let currentBalance = 200
let currentWorkPayBalance = 0
let debtAmount = 0
let haveLoan = false
let laptops = []


const imgBaseUrl = "https://noroff-komputer-store-api.herokuapp.com/"

// A function to make it easyer formating numbers to a currency.
const formatAmount = function (amount) {
    return new Intl.NumberFormat('da-DK',
        {
            style: 'currency',
            currency: 'DKK',
            maximumFractionDigits: 0
        })
        .format(amount)
}

// A function to update html values. 
function updateValues() {
    balance.innerHTML = formatAmount(currentBalance)
    workPayBalanceElement.innerHTML = formatAmount(currentWorkPayBalance)
    debtElement.innerHTML = formatAmount(debtAmount)
}

const isLoanActive = () => {
    if (debtAmount === 0) return false
    else return true
}

// Updates the UI with values and hides dept-element.
updateValues()
showOrHideDebtElement()

// Function used for checking if you are allowed to loan X amount. 
// Max is you current balance x 2.
const canHaveLoan = (ammount) => {
    maxLoanAmount = currentBalance * 2
    if (ammount > maxLoanAmount) return false
    else return true
}

// A function used to determin whether or not debt-element should show.
function showOrHideDebtElement() {
    if (isLoanActive()) {
        showElement(bankDebtElement)
        showElement(repayLoanButton)
    } else {
        hideElement(bankDebtElement)
        hideElement(repayLoanButton)
    }
}


// Function to control a loan.
function getLoan() {
    
    // If u already have a loan it throws an alert and returns.
    // Else it will promt how much you want.
    if (isLoanActive()) {
        alert("You loan request was declined.\n"
            + "You can not have more than one active loan.")
        return
    }

    // Promts you and saves your input as 'requestedAmount'
    requestedAmount = parseInt(prompt("How much do you need?"))
    if(Number.isNaN(requestedAmount) || requestedAmount < 1) {
        alert("Not a valid input")
        return
    }
    // Checks your input and alerts you if you are asking for too much.
    if (!canHaveLoan(requestedAmount)) {
        alert("You loan request was declined.\nAmount is too big.\n"
            + "You can not request more than double of yor current balance.")
        return
    }

    // Updates your balance and gives you a debt.
    currentBalance += requestedAmount
    debtAmount += requestedAmount
    haveLoan = true
    updateValues()
    showOrHideDebtElement()
}


// A function that pays for your work and update values.
function work() {
    const payment = 100
    currentWorkPayBalance += payment
    updateValues()

}

// Function to bank your money in the bank and automatically pays of your debt if u have one.
function bankMoney() {
    if (haveLoan) {
        let payBack = currentWorkPayBalance * .1
        if ((debtAmount - payBack) <= 0) {
            // Calculate the difference
            const difference = Math.abs(debtAmount - payBack)
            
            // Set the currentBalance minus the payback
            currentBalance += currentWorkPayBalance - payBack

            // Give back the difference
            currentBalance += difference

            // Set haveLoan to false and debtAmunt to 0
            haveLoan = false
            debtAmount = 0

            // Hide debt element
            showOrHideDebtElement()
        } else {

            // If non of the above - reduce the debt by 10% and give the remaings to the bank balance.
            debtAmount -= payBack
            currentBalance += currentWorkPayBalance - payBack
        }

    } else {

        // If you don't have a loan just add the pay to the bank balance.
        currentBalance += currentWorkPayBalance
    }

    // Reset the work-pay-balance and update values
    currentWorkPayBalance = 0
    updateValues()
}

function repayLoan() {
    if(debtAmount>=currentWorkPayBalance){
        debtAmount -= currentWorkPayBalance
        currentWorkPayBalance = 0        
    } else {
        currentWorkPayBalance -= debtAmount
        debtAmount = 0
    }
    updateValues()
    showOrHideDebtElement()
}

// ------------------------------

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
.then(response => response.json())
.then(data => laptops = data)
.then(laptops => addLaptopsToMenu(laptops))

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x))
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option")
    laptopElement.value = laptop.id
    laptopElement.appendChild(document.createTextNode(laptop.title))
    laptopsElement.appendChild(laptopElement)
}

const handleLaptopMenuChange = (e) => {
    let sepcsText = ""
    const selectedLaptop = laptops[e.target.selectedIndex]
    selectedLaptop.specs.forEach(feature => {
        sepcsText += feature + "\n"
    })

    // Updates the image according to the selected laptop.
    fetch(imgBaseUrl + selectedLaptop.image)
    .then((response)=>response.blob())
    .then((blob)=>{
       const objectUrl = URL.createObjectURL(blob)
       imageElement.src=objectUrl
    })
    .catch(error => console.log(error))

    priceElement.innerText = formatAmount(selectedLaptop.price)
    specsElement.innerText = sepcsText
    descriptionElement.innerText = selectedLaptop.description
}

laptopsElement.addEventListener("change", handleLaptopMenuChange)

function buy(){
    
    const currentPrice = parseInt(priceElement.innerText.replace('.', ""))
    
    if(currentBalance<currentPrice) alert("Not enough money 😥")
    else if(isNaN(currentPrice) || currentPrice === 0){
        alert("What do you wanna buy?🤔 ")

    }else{
        currentBalance -= currentPrice
        alert("Congrats on you new... laptop? 🤡")
    }

    updateValues()
    showOrHideDebtElement()
}



